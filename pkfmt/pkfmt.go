package main

import (
	"io/ioutil"
	"os"

	"github.com/panux/builder/pkgen"
	yaml "gopkg.in/yaml.v2"
)

func main() {
	dat, err := ioutil.ReadFile(os.Args[1])
	if err != nil {
		panic(err)
	}
	var rpg pkgen.RawPackageGenerator
	err = yaml.Unmarshal(dat, &rpg)
	if err != nil {
		panic(err)
	}
	dat, err = yaml.Marshal(rpg)
	if err != nil {
		panic(err)
	}
	err = ioutil.WriteFile(os.Args[1], dat, 0644)
	if err != nil {
		panic(err)
	}
}
