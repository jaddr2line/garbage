package main

import (
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/cookiejar"
)

func main() {
	cli := new(http.Client)
	jar, err := cookiejar.New(nil)
	if err != nil {
		panic(err)
	}
	cli.Jar = jar
	cli.CheckRedirect = func(req *http.Request, via []*http.Request) error {
		log.Println("redirect", req, via)
		return nil
	}
	req, err := http.NewRequest("GET", "https://sourceforge.net/projects/mad/files/libid3tag/0.15.1b/libid3tag-0.15.1b.tar.gz", nil)
	if err != nil {
		panic(err)
	}
	req.Header.Add("user-agent", "curl/7.60.0")
	req.Header.Add("accept", "*/*")
	log.Println("inital request", req)
	resp, err := cli.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	log.Println("response", resp)
	buf := make([]byte, 512)
	_, err = resp.Body.Read(buf)
	if err != nil {
		panic(err)
	}
	ctype := http.DetectContentType(buf)
	_, err = io.Copy(ioutil.Discard, resp.Body)
	if err != nil {
		panic(err)
	}
	log.Println(ctype)
}
