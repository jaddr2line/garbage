package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"
)

func a(s string) string {
	return strings.Replace(s, ":", "-", -1)
}

func main() {
	var g map[string][]string
	err := json.NewDecoder(os.Stdin).Decode(&g)
	if err != nil {
		log.Fatalf("Failed to load graph: %q", err)
	}
	buf := bufio.NewWriter(os.Stdout)
	buf.WriteString("digraph G {\n")
	for n, d := range g {
		buf.WriteString(fmt.Sprintf("\tall -> %q;\n", a(n)))
		for _, v := range d {
			buf.WriteString(fmt.Sprintf("\t%q -> %q;\n", a(n), a(v)))
		}
	}
	buf.WriteString("}\n")
	err = buf.Flush()
	if err != nil {
		log.Fatalf("Failed to write graph: %q", err)
	}
}
