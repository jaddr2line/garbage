package main

import (
	"flag"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"
)

func main() {
	flag.StringVar(&old, "old", "", "old text")
	flag.StringVar(&new, "new", "", "new text")
	var base string
	flag.StringVar(&base, "pkg", "", "package to modify")
	flag.Parse()

	base = filepath.Join(os.Getenv("GOPATH"), "src", base)

	wg.Add(1)
	/*go*/ fsRecurse(base)
	//wg.Wait()
}

var wg sync.WaitGroup

var old string
var new string

func migrateIt(fname string) {
	log.Printf("Migrating %q\n", fname)
	//meh:
	f, err := os.OpenFile(fname, os.O_RDWR, 0)
	if err != nil {
		/*if strings.Contains(err.Error(), "too many open files") { //overload - backoff
			time.Sleep(time.Second / 4)
			goto meh
		}*/
		log.Printf("failed to migrate file %q: %s\n", fname, err.Error())
		return
	}
	dat, err := ioutil.ReadAll(f)
	if err != nil {
		log.Printf("failed to migrate file %q: %s\n", fname, err.Error())
		return
	}
	_, err = f.Seek(0, 0)
	if err != nil {
		log.Printf("failed to migrate file %q: %s\n", fname, err.Error())
		return
	}
	_, err = f.Write([]byte(strings.Replace(string(dat), old, new, -1)))
	if err != nil {
		log.Printf("failed to migrate file %q: %s\n", fname, err.Error())
		return
	}
}

func fsRecurse(path string) {
	//defer wg.Done()
	//meh:
	log.Printf("Scanning %q\n", path)
	f, err := os.Open(path)
	if err != nil {
		/*if strings.Contains(err.Error(), "too many open files") { //overload - backoff
			time.Sleep(time.Second / 8)
			goto meh
		}*/
		log.Printf("open error on %q: %s\n", path, err.Error())
		return
	}
	defer f.Close()
	info, err := f.Stat()
	if err != nil {
		log.Printf("stat error on %q: %s\n", path, err.Error())
		return
	}
	if info.IsDir() {
		dirinf, err := f.Readdir(-1)
		if err != nil {
			log.Printf("readdir error on %q: %s\n", path, err.Error())
			return
		}
		for _, v := range dirinf {
			wg.Add(1)
			/*go*/ fsRecurse(filepath.Join(path, v.Name()))
		}
	} else if filepath.Ext(info.Name()) == ".go" {
		migrateIt(path)
	}
}
